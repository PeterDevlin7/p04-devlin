//
//  GameView.h
//  The-Adventures-of-Leggy-Boy
//
//  Created by Peter Devlin on 3/11/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LeggyBoy.h"
#import "Brick.h"

@interface GameView : UIView

@property (nonatomic, strong) LeggyBoy *lb;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic) BOOL tapped;
@property (nonatomic, strong) IBOutlet UIViewController *vc;
@property (nonatomic, strong) IBOutlet UILabel *scoreLabel;

-(void)arrange:(CADisplayLink *)sender;
-(void)setDimensions:(float)height width:(float) width;
-(void)startBricks;

@end
