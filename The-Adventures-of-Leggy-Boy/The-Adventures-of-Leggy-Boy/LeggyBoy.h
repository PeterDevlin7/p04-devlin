//
//  LeggyBoy.h
//  The-Adventures-of-Leggy-Boy
//
//  Created by Peter Devlin on 3/11/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LeggyBoy : UIView

@property (nonatomic) float dx, dy;  // Velocity
@property (nonatomic) CGRect rect;   // Frame

@end
