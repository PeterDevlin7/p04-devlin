//
//  GameView.m
//  The-Adventures-of-Leggy-Boy
//
//  Created by Peter Devlin on 3/11/17.
//  Copyright © 2017 Peter Devlin. All rights reserved.
//

#import "GameView.h"

@implementation GameView

@synthesize lb, bricks;
@synthesize tapped;
@synthesize vc;
@synthesize scoreLabel;
float boundaryMargin = 10.0;
float actualHeight;
float actualWidth;
float height = 70;
int score = 0;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    return self;
}

-(void)setDimensions:(float) height width:(float) width {
    actualHeight = height;
    actualWidth = width;
    //NSLog(@"Actual height: %f\nActual width: %f", actualHeight, actualWidth);
}

-(void)startBricks
{
    
    lb = [[LeggyBoy alloc] initWithFrame:CGRectMake(actualWidth/2, actualHeight - 20, 20, 20)];
    [lb setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"ant.jpg"]]];
    [lb setDx:0];
    [lb setDy:0];
    [self addSubview:lb];
    
    bricks = [[NSMutableArray alloc] init];
    Brick *floor = [[Brick alloc] initWithFrame:CGRectMake(0, 0, actualWidth, height/7)];
    [floor setBackgroundColor:[UIColor blackColor]];
    [floor setCenter:CGPointMake(actualWidth/2, actualHeight)];
    [self addSubview:floor];
    [self generateBrick];
    
}

-(void)restart
{
    score = 0;
    [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", score]];
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
            [bricks removeObject:brick];
        }
    }
}
/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect {
 // Drawing code
 }
 */

-(void)arrange:(CADisplayLink *)sender
{
    // Apply gravity to the acceleration of the lb
    if(lb.center.y < actualHeight - 10)[lb setDy:[lb dy] - .3];
    // Move lb up if tapped
    if(tapped && lb.center.y == actualHeight - 10) [lb setDy:8];
    
    // lb moves in the direction of gravity
    CGPoint p = [lb center];
    p.x += [lb dx];
    p.y -= [lb dy];
    if(p.y > actualHeight - 10) p.y = actualHeight - 10;
    [self moveBricksLeft];
    
    // If we have gone too far left, or too far right, stop at the side
    if (p.x < boundaryMargin) {
        p.x = boundaryMargin;
    }
    if (p.x > actualWidth - boundaryMargin) {
        p.x = actualWidth - boundaryMargin;
    }
    // Detect collision
    CGPoint topRight = CGPointMake(p.x + 10, p.y - 10);
    CGPoint bottomRight = CGPointMake(p.x + 10, p.y + 10);
    for (Brick *brick in bricks)
    {
        CGRect b = [brick frame];
        if (CGRectContainsPoint(b, topRight) || CGRectContainsPoint(b, bottomRight) || CGRectContainsPoint(b, p))
        {
            [self restart];
            break;
        }
    }
    [lb setCenter:p];
    tapped = false;
}

-(void)moveBricksLeft
{
    
    NSMutableArray *removeBricks;
    removeBricks = [[NSMutableArray alloc] init];
    for (Brick *brick in bricks) {
        CGPoint p = [brick center];
        p.x -= 2;
        [brick setCenter:p];
        if(p.x < 0) [removeBricks addObject: brick];
    }
    for (Brick *brick in removeBricks) {
        [bricks removeObject:brick];
        [brick removeFromSuperview];
        [self generateBrick];
        score++;
        [scoreLabel setText:[NSString stringWithFormat:@"Score: %d", score]];
    }
    if(bricks.count == 0) [self generateBrick];
}

-(void)generateBrick
{
    
    Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, 10, height)];
    [b setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"platform.png"]]];
    [self addSubview:b];
    [b setCenter:CGPointMake(actualWidth, actualHeight - (height/2))];
    [bricks addObject:b];
}
@end
